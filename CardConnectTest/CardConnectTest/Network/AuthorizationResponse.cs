﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CardConnectTest.Network
{
    [DataContract]
    class AuthorizationResponse
    {
        // Potentially expand to enum or something? ABC not really great readability
        [DataMember(Name = "respstat")]
        public String ResponseStatus { get; set; }
        [DataMember(Name = "retref")]
        public int RetrievalReferenceNumber { get; set; }
        [DataMember(Name = "account")]
        public int AccountNumber { get; set; }
        [DataMember(Name = "token")]
        public String CardNumberToken { get; set; }
        [DataMember(Name = "amount")]
        public Decimal Amount { get; set; }
        [DataMember(Name = "merchId")]
        public String MerchantId { get; set; }
        [DataMember(Name = "respcode")]
        public String ResponseCode { get; set; }
        [DataMember(Name = "resptext")]
        public String ResponseText { get; set; }
        [DataMember(Name = "respproc")]
        public String ResponseProcessor { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardConnectTest.Network
{
    interface ICardConnectClient
    {
        AuthorizationResponse Authorize(AuthorizationRequest authorizationRequest);
    }
}

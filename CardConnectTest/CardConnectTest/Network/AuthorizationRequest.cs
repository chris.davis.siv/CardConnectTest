﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace CardConnectTest.Network
{
    [DataContract]
    public class AuthorizationRequest
    {
        [DataMember(Name ="merchId", IsRequired =true)]
        public String MerchID { get; set; }
        [DataMember(Name = "amount", IsRequired =true)]
        public Decimal Amount { get; set; }
        [DataMember(Name = "currency", IsRequired = true)]
        public String Currency { get; set; }
        [DataMember(Name = "expiry", IsRequired =true)]
        public String ExpiryDate { get; set; }
        [DataMember(Name = "account", IsRequired=true)]
        public String AccountNumber { get; set; }

        [DataMember(Name = "bankaba")]
        public int BankABANumber { get; set; }
        [DataMember(Name = "track")]
        public String TrackData { get; set; }
        [DataMember(Name = "profile")]
        public String Profile { get; set; }
        [DataMember(Name = "acctid")]
        public int AcctIid { get; set; }
        [DataMember(Name = "capture")]
        public bool Capture { get; set; }
        [DataMember(Name = "tokenize")]
        public bool Tokenize { get; set; }
        [DataMember(Name = "signature")]
        public String SignatureData { get; set; }
        [DataMember(Name = "cvv2")]
        public int CVV2 { get; set; }
        [DataMember(Name = "name")]
        public String AccountName { get; set; }
        [DataMember(Name = "address")]
        public String AccountStreetAddress { get; set; }
        [DataMember(Name = "city")]
        public String AccountCity { get; set; }
        [DataMember(Name = "postal")]
        public String PostalCode { get; set; }
        [DataMember(Name = "region")]
        public String Region { get; set; }
        [DataMember(Name = "country")]
        public String Country { get; set; }
        [DataMember(Name = "phone")]
        public String PhoneNumber {get; set; }
        [DataMember(Name = "email")]
        public String EmailAddress { get; set; }
        [DataMember(Name = "orderid")]
        public String OrderId { get; set; }
        [DataMember(Name = "authcode")]
        public String AuthCode { get; set; }
        [DataMember(Name = "taxexempt")]
        public bool TaxExempt { get; set; }
        [DataMember(Name = "termid")]
        public String terminalDeviceId { get; set; }
        [DataMember(Name = "accttype")]
        public String AccountType { get; set; }
    }


}

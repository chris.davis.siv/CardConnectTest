﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardConnectTest.Mocks
{
    public class MockCustomer
    {
        public readonly int AccountNumber;
        public Decimal CardLimit { get; set; }
        private Decimal CurrentBalance { get; set; }

        public MockCustomer(int accountNumber, Decimal cardLimit)
        {
            CurrentBalance = 0;
            AccountNumber = accountNumber;
            CardLimit = cardLimit;
        }
        public void PayOffCard()
        {
            CurrentBalance = 0;
        }
        public Decimal GetRemainingBalance()
        {
            return CardLimit - CurrentBalance;
        }
        public void Debit(Decimal amount)
        {
            CurrentBalance += amount;
        }
    }
}

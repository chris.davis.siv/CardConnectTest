﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardConnectTest.Network;

namespace CardConnectTest.Mocks
{
    class MockClient : ICardConnectClient
    {
        public Dictionary<String, MockCustomer> customerDatabase;
        public Dictionary<String, MockTransaction> transactionDatabase;

        public AuthorizationResponse Authorize(AuthorizationRequest authorizationRequest)
        {
            //Debit request
            if (authorizationRequest.Amount > 0)
            {

            }
            //Auth check
            if (authorizationRequest.Amount == 0)
            {

            }
        }
    }
}
